/*
AJAX call using URL on GeoServer, returns the features as
an array of GeoJSON.
*/
function fetchGeoJSONFeats(url){

  var features;

  /* Get the GeoJSON, return a list of features to match with the
  vector tiles ID. This will get the hover and text set style
  to prevent the re-drawing of the layer every time
  via a style function*/

    $.ajax(url,{
    type:'GET',
    async: false // This allows for the array of data to load before the map
  }).done(function (geojson){

    features = new ol.format.GeoJSON().readFeatures(geojson);
  });

  //Return the array of features
  return features;
}

function updateWFS(tableName, currentFeat, status, notes){

  console.log(tableName);
  console.log(currentFeat);
  console.log(status);
  console.log(notes);

  //set values to the feature you are sending to server
  currentFeat[0].values_.status = status;
  currentFeat[0].values_.notes = notes;


  var formatWFS = new ol.format.WFS();
  var formatGML = new ol.format.GML({
    url: 'http://localhost:8080/geoserver/wfs',
    featureNS: 'http://letchworth_geoserver.org',
    featureType: tableName, // replace with relevant feat
    srsName: 'EPSG:4326'

  });

  var xs = new XMLSerializer();

  node = formatWFS.writeTransaction(null, currentFeat, null, formatGML);

  console.log(node);
  var payload = xs.serializeToString(node);
  console.log(payload);

  //Use a regular expression so that geometry is not inserted.
    var regex = new RegExp("(<Property>)(.+?)(geometry)(.+?)(<\/Property>)"); //regex had wkb_geometry

    var replace = "<Property>";

    var newStr = payload.replace(regex, '');

    console.log(newStr);


  $.ajax('http://localhost:8080/geoserver/wfs', {
    service: 'WFS',
    type:'POST',
    dataType:'xml',
    processData: false,
    contentType: 'text/xml',
    data: newStr
  }).done(function(e){
    console.log(e);
    console.log("done");
    //sourceWFS.clear();
  });





}

/*
Creates a table to render in sidebar based on
parameters from the geojson feats
*/
function buildTableArray(){

  var features = fetchGeoJSONFeats('http://192.168.1.157:8080/geoserver/wfs?service=WFS&'
  +'version=1.1.0&request=GetFeature&typename=Letchworth_Park:trails'
  +'&outputFormat=application/json&srsname=EPSG:4326');

  console.log(features);

  var objectArray = [];
  var assetTracker = [];

  //Create a table with the resulting trails (no duplicates)
  for(i = 0; i < features.length; i++){

    // name must not already exist
    // if number is the same, add length property
    // append bike,trail,snowboard, etc
    // append condition
    // append difficulty

    var newObj;
    var layer = features[i];
    //console.log(layer.values_.name);
    var name = layer.values_.name;

    //console.log(objectArray.length);
    if (i == 1){
      //console.log(objectArray);
    }

    if (objectArray.find(x => x.number === layer.values_.abbrev) === undefined) {
      //console.log("UNDEFINED");
      assetTracker.push(layer.values_.abbrev);
    }
    else{
      // already exists, append mileage for curr obj
      //console.log("FOUND");
      //console.log(layer.values_.abbrev);
      objectArray.find(x => x.number === layer.values_.abbrev).mileage += layer.values_.miles;
      continue;

    }
    var number = layer.values_.abbrev;
    var bike;
    if (layer.values_.bike == "N"){
      bike = "No";
    }
    else {
      bike = "Yes";
    }
    var atv;
    if (layer.values_.atv == "N"){
      atv = "No";
    }
    else {
      atv = "Yes";
    }
    var horse;
    if (layer.values_.horse == "N"){
      horse = "No";
    }
    else {
      horse = "Yes";
    }
    var snowMobile
    if (layer.values_.snowmb == "N"){
      snowMobile = "No";
    }
    else {
      snowMobile = "Yes";
    }

    var status = layer.values_.status;
    var surface = layer.values_.surface;
    var mileage = layer.values_.miles;

    // Only include trails with distance
    if (mileage > .5){
      var newObject = {name:name,number:number,bike:bike,atv:atv,horse:horse,snowMobile:snowMobile,status:status,surface:surface,mileage:mileage}
      //console.log(newObject);
      objectArray.push(newObject);
    }


  };


/*
    var cql = "STATE_NAME='Wisconsin'";

  var wmsStateBorder = new ol.layer.Image({
          source: new ol.source.ImageWMS({
            url: 'http://192.168.1.157:8080/geoserver/wms',
            params: {'LAYERS': 'topp:states', 'cql_filter': cql},
            ratio: 1,
            serverType: 'geoserver'
          })
        });
        */

        /*
        well_nitrates_wms.getSource().updateParams({'LAYERS': 'Cancer_Nitrate_Rates:Nitrate_Redux'});
        well_nitrates_wms.getSource().refresh();
        */

  // Put this array in a mothereffin table
  //console.log(objectArray);

  /*
  var table = new Tabulator('#example-table',{
    height:"50%",
    layout:"fitColumns",
    placeholder:"No Data Set",
    columns:[
      {title:"Name",field:"name",sorter:"string",width:50},
      //{title:"number",field:"number",sorter:"string",width:50},
      //{title:"bike",field:"bike",sorter:"string",width:50},
      //{title:"atv",field:"atv",sorter:"string",width:50},
      //{title:"horse",field:"horse",sorter:"string",width:50},
      //{title:"snowMobile",field:"snowMobile",sorter:"string",width:50},
      //{title:"status",field:"status",sorter:"string",width:50},
      //{title:"surface",field:"surface",sorter:"string",width:50},
      {title:"Miles",field:"mileage",sorter:"number",width:50},

    ],
    rowClick:function(e,row){
      clearStyleSelector();
      // For the row, zoom and highlight trail
      var currName = row._row.data.name;
      // Match it in the table, zoom to it
      var currFeat = findFeat(currName);
    },
  });

  table.setData(objectArray);
  */






}

function clearOptions(){
  // Clear all the options from the form control for searching for a feature.
  console.log($("#amenityFinder"));
  $("#amenityFinder").find('option').remove().end();

}

function enableDropdown(wfsFeature,featName){
  // Load up the choppa with trail features
  var dropdownOptions = wfsFeature.getSource().getFeatures();
  console.log(dropdownOptions);

  var mapVals = {};

  // for each trail, append name as dropdown option
  for (i = 0; i < dropdownOptions.length; i++){
    console.log("loading vals");
    console.log(mapVals);
    //mapVals.push({key:trailOptions[i].values_.name,
      //value:trailOptions[i].values_.geometry.extent_});
      mapVals[dropdownOptions[i].values_[featName]] = dropdownOptions[i].values_.geometry.extent_;
      //console.log(dropdownOptions[i].values_[featName]);
      var name = dropdownOptions[i].values_[featName];
      var geom = dropdownOptions[i].values_.geometry.extent_
      //console.log(geom);
      $("#amenityFinder").append($('<option>', {name,geom})
      .text(name));


  }
  return mapVals;


}

function findFeat(currName){

      var trailz = trailLabelFeats.getSource().getFeatures();
      //console.log(trailz);
      for (i = 0; i < trailz.length; i++){
        var layer = trailz[i];
        if (layer.values_.name == currName){
          layer.setStyle(styleHover);
          //console.log(layer);
          var extent = layer.values_.geometry.extent_;
          map.getView().fit(extent,map.getSize());
        }


    };



}
