<%--
  Created by IntelliJ IDEA.
  User: thoma
  Date: 3/24/2018
  Time: 1:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <meta charset=UTF-8>
    <meta name="viewport" content="width=device-width,
                               initial-scale=1">
    <title>Compare OSM and Gov Data</title>

    <%--Include style sheet--%>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="https://openlayers.org/en/v4.6.5/css/ol.css" type="text/css">

    <!-- UNKNOWN -->
    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=requestAnimationFrame,Element.prototype.classList,URL">
    </script>

    <!-- JQUERY -->
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- OpenLayers Quickstart recommends loading only components: look into that-->
    <script src="https://openlayers.org/en/v4.6.5/build/ol.js" type="text/javascript"></script>

  </head>

  <body>


  <nav class="navbar navbar-inverse">
    <div class="container-fluid"></div>
    <div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar" aria-exapanded="false">
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
  </button>
      <a class="navbar-brand" href="#">City of Alexandria Data Editor</a>
    </div>


    <div class="collapse navbar-collapse" id="myNavbar">
    <ol class="nav navbar-nav navbar-right">
        <div class="dropdown2">
            <button class="btn btn-default dropdown-toggle" id="exportButton" type="button" data-toggle="dropdown">Export
                <span class="caret"></span></button>
            <ul class="dropdown-menu" id="exportDrop">
                <li><a href="http://localhost:8080/geoserver/wfs/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=th_geoserver:esri_parks&outputformat=SHAPE-ZIP">Parks</a></li>
                <li><a href="http://localhost:8080/geoserver/wfs/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=th_geoserver:esri_buildings&outputformat=SHAPE-ZIP">Footprints</a></li>
                <li><a href="http://localhost:8080/geoserver/wfs/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=th_geoserver:esri_roads&outputformat=SHAPE-ZIP">Roads</a></li>
                <li><a href="http://localhost:8080/geoserver/wfs/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=th_geoserver:esri_trails&outputformat=SHAPE-ZIP">Trails</a></li>
            </ul>
        </div>
      <div class="dropdown">
        <button class="btn btn-default dropdown-toggle" id="dank" type="button" id="menu1" data-toggle="dropdown">Select Mode
          <span class="caret"></span></button>
        <ul class="dropdown-menu" id="modeDrop" role="menu" aria-labelledby="menu1">
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Browse</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Edit Layer</a></li>
          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Flag AOI</a></li>
          <li role="presentation" class="divider"></li>
          <li role="presentation"><a data-toggle="modal" role="menuitem" tabindex="-1" href="#helpmodal">Help</a></li>
        </ul>
      </div>
        <div class="modal fade" id="helpmodal" data-target="#helpmodal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header orange">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>

                        </button>
                        <h2 class="modal-title"><strong></strong>Guide For Using Data Editor</h2>
                        <p>    Welcome to the GIS data manager for the city of Alexandria, VA!<br>Follow the prompt below for an overview on how to use each tool:</p>
                        <ul>
                            <li><b>Flag AOI: </b>Flag a disputed area in your database.</li>
                            <b>How:</b> Select the tab and place a point on the government map (Left). Enter parameters and submit."
                            <li><b>Edit Layer:</b> Make attribute-based changes to government map features.</li>
                            <b>How:</b> Select the tab and click on the government map to retrieve a from of attributes to edit. Make changes and submit.
                            <li><b>Browse:</b> Compare the difference in attributes between the two data sources.</li>
                            <b>How:</b> Select the tab and click on one of the two maps. A popup will render on each map showing the feature attribution at that coordinate.
                            <li><b>Export:</b> Save altered government data to your PC.</li>
                            <b>How:</b> Select one of the four government data layers (Parks, Trails, Footprints, Roads) from the dropdown. Download will start automatically.

                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </ol>
    </div>
  </nav>

  <div class="container nomargin">
  <div id ="map1" class="half-map">
      <div id="mapLegendOSM">
          <img src="http://localhost:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=15&LAYER=th_geoserver:osm_layergroup&LEGEND_OPTIONS=fontName:Ubuntu;colums:2">
      </div>
          <div id="popup"class="ol-popup">
      <div id="popup-content"></div>
    <div id="popup-closer"class="ol-popup-closer" ></div>
  </div>
    </div>
    <div id="map2" class="half-map">
        <div id="mapLegend">
            <img src="http://localhost:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=15&LAYER=th_geoserver:esri_layergroup&LEGEND_OPTIONS=fontName:Ubuntu;columns:2">
        </div>
      <div id="popup2"class="ol-popup">
        <div id="popup-content2"></div>
        <div id="popup-closer2"class="ol-popup-closer"></div>
  </div>
      <div id="popup3"class="ol-popup">

            <h4>Select Input Feature</h4>
            <div data-toggle="buttons">
                <div class="row">
                    <div class="col-sm-12">
                        <label class="col-sm-4 radio-inline"><input type="radio" name="optradio" value="parks">Parks</label>
                        <label class="col-sm-4 radio-inline"><input type="radio" name="optradio" value="trails">Trails</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <label class="col-sm-4 radio-inline"><input type="radio" name="optradio" value="footprints">Footprints</label>
                        <label class="col-sm-4 radio-inline"><input type="radio" name="optradio" value="roads">Roads</label>
                    </div>
                </div>


            </div>

            <form class="form"><h4>Comment</h4><input id="comment" type="text"/></form>
            <div class="row popup-btns">
                <div class="col-sm-4 success-btn">
                    <button type="button" class="btn btn-success" id="submitbtn">Submit</button>
                </div>
                <div class="col-sm-4">
                    <button type="button" class="btn btn-danger" id="cancelbtn">Cancel</button>
                </div>
            </div>

      </div>

         <%-- <h4>Add Feature!</h4>
          <form>
            <label class="radio-inline">
              <input type="radio" name="optradio" value="parks">Park
            </label>
            <label class="radio-inline">
              <input type="radio" name="optradio" value="trails">Trail
            </label>
            <label class="radio-inline">
              <input type="radio" name="optradio" value="footprints">Building
            </label>
            <label class="radio-inline">
              <input type="radio" name="optradio" value="roads">Road
            </label>
            <div class = "form-group">
            <label for="comment">Comment:</label>
            <input type="text" class="form-control" id="comment">
            </div>
            <div class="submitbtn">
            <button type="button" class="btn btn-default">Submit</button>
        </div>
          </form>--%>
        <div id="popup-content3"></div>
        <%--<div id="popup-closer3"class="ol-popup-closer"></div>--%>
        </div>
        <%--<div id="popup-content3"></div>
        <div id="popup-closer3"class="ol-popup-closer"></div>--%>

      <div class="formInput">
          <div id="formInfo"></div>
          <%--<h2>Edit Feature</h2>
          <form>--%>
              <%--<div class="form-group">
                  <label for="inputdefault">FID</label>
                  <input class="form-control" placeholder="FOOTLOOSE" id="inputdefault" type="text">
              </div>
              <div class="form-group">
                  <label for="inputlg">input-lg</label>
                  <input class="form-control input-lg" id="inputlg" type="text">
              </div>
              <div class="form-group">
                  <label for="inputsm">input-sm</label>
                  <input class="form-control input-sm" id="inputsm" type="text">
              </div>
              <div class="form-group">
                  <label for="sel1">Default select list</label>
                  <select class="form-control" id="sel1">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                  </select>
              </div>
              <div class="form-group">
                  <label for="sel2">input-lg</label>
                  <select class="form-control input-lg" id="sel2">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                  </select>
              </div>
              <div class="form-group">
                  <label for="sel3">input-sm</label>
                  <select class="form-control input-sm" id="sel3">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                  </select>
              </div>--%>
          <%--</form>--%>
      </div>
    </div>
      <div class="row">
          <div class="col" id="info"></div>
      </div>
  </div>


    <script src = "js/loadMaps.js"></script>
    <script src="js/editUtils.js"></script>
  </body>
</html>


   <%--   $(".dropdown-menu li a").click(function(){
          console.log("HEY");
          $("#dank:first-child").text($(this).text());
          $("#dank:first-child").val($(this).text());
      });


      var container = document.getElementById('popup');
      var container2 = document.getElementById('popup2');

      var content = document.getElementById('popup-content');
      var content2 = document.getElementById('popup-content2');

      var closer = document.getElementById('popup-closer');
      var closer2 = document.getElementById('popup-closer2');


      var overlay = new ol.Overlay({
          element:container
      });

      var overlay2 = new ol.Overlay({
          element:container2
      });

      closer.onclick = function(){
          overlay.setPosition(undefined);
          closer.blur();
          return false;
      }

      closer2.onclick = function(){
          overlay2.setPosition(undefined);
          closer2.blur();
          return false;
      }

      var wmsSource = new ol.source.ImageWMS({
          ratio: 1,
          url: 'http://localhost:8080/geoserver/learnGeoServer/wms',
          params: {'VERSION':'1.1.1',
              STYLES:'',
              LAYERS: 'learnGeoServer:osmParks_Buildings',}
      });

      var wmsSourceESRI = new ol.source.ImageWMS({
          ratio:1,
          url: 'http://localhost:8080/geoserver/learnGeoServer/wms',
          params:{'VERSION':'1.1.1',
          STYLES:'',
          LAYERS: 'learnGeoServer:esriParks_Buildings'}
      });

      var mapLayers = [
          new ol.layer.Tile({
              source: new ol.source.BingMaps({
                  key: 'Aig2eNz6WHGMJnqkZLAqb rKi4MZE7Ve0qSmwu8I-xuulGN_Hl24jLi_VA_3qAuMd',
                  imagerySet:'Aerial',
              })
          }),
          new ol.layer.Image({
              source:wmsSource
          })
      ];


      var esriLayers = [mapLayers[0],
      new ol.layer.Image({
          source:wmsSourceESRI
      })];

     var view = new ol.View({
          center: [-77.079742,38.819028],
          zoom: 16,
          projection: new ol.proj.Projection({
              code:'EPSG:4326',
              units:'degrees',
              axisOrientation: 'neu',
              global: true
          })
      });



      var map1 = new ol.Map({
          layers:mapLayers,
          overlays:[overlay],
          target:'map1',
          view: view
      });


      var map2 = new ol.Map({
          layers:esriLayers,
          overlays:[overlay2],
          target:'map2',
          view: view
      });

      map1.on('singleclick',function(evt){
          var coordinate = evt.coordinate;
          var viewResolution = (map1.getView().getResolution());
          var viewResolution2 = (map2.getView().getResolution());

          var url = wmsSource.getGetFeatureInfoUrl(
              evt.coordinate, viewResolution, 'EPSG:4326',
              {'INFO_FORMAT': 'application/json'});


          // currently used for osm popup
          var url2 = wmsSource.getGetFeatureInfoUrl(
              evt.coordinate, viewResolution, 'EPSG:4326',
              {'INFO_FORMAT': 'text/html'});

          console.log(coordinate);

          // esri request
          var esriURL = wmsSourceESRI.getGetFeatureInfoUrl(
              coordinate, viewResolution2, 'EPSG:4326',
              {'INFO_FORMAT':'text/html'});


          console.log(evt.coordinate);


          if (url2) {
              console.log("TRUE");

              // set popup for osm map
              content.innerHTML =
                  '<iframe seamless src="' + url2 + '"></iframe>';
              overlay.setPosition(evt.coordinate);

              if (esriURL) {

                  // set popup for esri map
                  content2.innerHTML = '<iframe seamless src="' + esriURL + '"></iframe>';
                  overlay2.setPosition(evt.coordinate);

              }

          }


          if (!url){
              var parser = new ol.format.GeoJSON();

              $.ajax({
                  url: url,
                  dataType: 'json',
                  jsonpCallback: 'parseResponse'
              }).then(function(response){
                  console.log("here");
                  var result = parser.readFeatures(response);
                  //console.log(JSON.stringify(result));
                  //console.log(JSON.stringify(response));

                  // Get the geometry text because I'm giving up tonight.

                  var info = [];
                  console.log("RES LEN");
                  console.log(result.length);
                  for (var i = 0, ii = result.length; i < ii; ++i){

                      info.push(result[i]);
                      console.log(result[i]);

                  }

                  console.log(info[0].a);
                  console.log(info[0].N.leisure);

                  $(element).popover('destroy');

                  var giveme = "PARK: " + info[0].N.leisure + "\nAMENITY:" + info[0].N.amenity;

                  popup.setPosition(coordinate);

                  $(element).popover({
                      'placement': 'top',
                      'animation': false,
                      'html':true,
                      'content': giveme

                  });

                  $(element).popover('show');





                  // Get the values you need in life.

                  //info is an array of objects
                  console.log(info);


                  //console.log(result);

              })
          };

      });

      // map 2
      map2.on('singleclick',function(evt){
          var coordinate = evt.coordinate;
          var viewResolution = (map2.getView().getResolution());
          var viewResolution2 = (map1.getView().getResolution());

          var url = wmsSourceESRI.getGetFeatureInfoUrl(
              evt.coordinate, viewResolution, 'EPSG:4326',
              {'INFO_FORMAT': 'application/json'});


          // currently used for osm popup
          var url2 = wmsSourceESRI.getGetFeatureInfoUrl(
              evt.coordinate, viewResolution, 'EPSG:4326',
              {'INFO_FORMAT': 'text/html'});

          console.log(coordinate);

          // esri request
          var osmUrl = wmsSource.getGetFeatureInfoUrl(
              coordinate, viewResolution2, 'EPSG:4326',
              {'INFO_FORMAT':'text/html'});


          console.log(evt.coordinate);


          if (url2) {
              console.log("TRUE");

              // set popup for osm map
              content2.innerHTML =
                  '<iframe seamless src="' + url2 + '"></iframe>';
              overlay2.setPosition(evt.coordinate);

              if (osmUrl) {

                  // set popup for esri map
                  content.innerHTML = '<iframe seamless src="' + osmUrl + '"></iframe>';
                  overlay.setPosition(evt.coordinate);

              }

          }

      });--%>
