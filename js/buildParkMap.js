var restroom_facilities;
var cabin_facilities;
var trails_wms;
var trails_highlight_wms;
var styleHover;
var selectElement;
var map;
var trailTableJSON;
var maxTrailDistance = 0.0;
var currentFeatEdit;
var entrances_wms;
var railroads_wms;
var admin_wms;
var info_kiosk_wms;
var editTrailDataSelection;
var editEntranceDataSelection;
var editCampgroundLocationSelection;
var campgroundLocations;
var campgroundWFS;
var mapVals;
var trailOverlay;
var campgroundOverlay;
var overlooks_wms;
var overlookWFS;
var tourist_locations;
var tourist_locations_wfs;

var container;
var content;
var closer;

/*Where the map is created */
function createMap(){

  // Get the HERE MAP
  var hereLayer = getHERELayer();

  var hereTile = new ol.layer.Tile({
    source: new ol.source.XYZ({
      url: hereLayer,
    }),
    name: 'basemap'
  });

  // Build map
  map = new ol.Map({
    target: 'map',
    layers: [hereTile],
    view: new ol.View({
      center: ol.proj.fromLonLat([-77.979765,42.639891]),
      zoom: 12,
      minzoom: 6,
      maxzoom: 15,
      /*extent: ol.proj.transformExtent([-93.0,42.4,-86.5,47.3], 'EPSG:4326','EPSG:3857')*/
    })
  });


  var sidebar = new ol.control.Sidebar({ element: 'sidebar', position: 'left' });
  map.addControl(sidebar);

  restroom_facilities = new ol.layer.Image({
          source: new ol.source.ImageWMS({
            url: 'http://192.168.1.157:8080/geoserver/wms',
            params: {'LAYERS': 'Letchworth_Park:comfort_stations'},
            zIndex:2,
            ratio: 1,
            serverType: 'geoserver'
          })
        });

  map.addLayer(restroom_facilities);

  cabin_facilities = new ol.layer.Image({
          source: new ol.source.ImageWMS({
            url: 'http://192.168.1.157:8080/geoserver/wms',
            params: {'LAYERS': 'Letchworth_Park:cabins'},
            zIndex:2,
            ratio: 1,
            serverType: 'geoserver'
          })
        });

  map.addLayer(cabin_facilities);

  trails_wms = new ol.layer.Image({
          source: new ol.source.ImageWMS({
            url: 'http://192.168.1.157:8080/geoserver/wms',
            params: {'LAYERS': 'Letchworth_Park:trails'},
            zIndex:2,
            ratio: 1,
            serverType: 'geoserver'
          })
        });
  map.addLayer(trails_wms);

  entrances_wms = new ol.layer.Image({
          source: new ol.source.ImageWMS({
            url: 'http://192.168.1.157:8080/geoserver/wms',
            params: {'LAYERS': 'Letchworth_Park:entrances'},
            zIndex:2,
            ratio: 1,
            serverType: 'geoserver'
          })
        });
  map.addLayer(entrances_wms);

  railroad_wms = new ol.layer.Image({
          source: new ol.source.ImageWMS({
            url: 'http://192.168.1.157:8080/geoserver/wms',
            params: {'LAYERS': 'Letchworth_Park:railroad'},
            zIndex:1,
            ratio: 1,
            serverType: 'geoserver'
          })
        });
  map.addLayer(railroad_wms);

  admin_wms = new ol.layer.Image({
          source: new ol.source.ImageWMS({
            url: 'http://192.168.1.157:8080/geoserver/wms',
            params: {'LAYERS': 'Letchworth_Park:Admin_Buildings'},
            zIndex:1,
            ratio: 1,
            serverType: 'geoserver'
          })
        });
  map.addLayer(admin_wms);

  info_kiosk_wms = new ol.layer.Image({
          source: new ol.source.ImageWMS({
            url: 'http://192.168.1.157:8080/geoserver/wms',
            params: {'LAYERS': 'Letchworth_Park:information_kiosks'},
            zIndex:1,
            ratio: 1,
            serverType: 'geoserver'
          })
        });
  map.addLayer(info_kiosk_wms);

  overlooks_wms = new ol.layer.Image({
          source: new ol.source.ImageWMS({
            url: 'http://192.168.1.157:8080/geoserver/wms',
            params: {'LAYERS': 'Letchworth_Park:overlooks'},
            zIndex:2,
            ratio: 1,
            serverType: 'geoserver'
          })
        });
  map.addLayer(overlooks_wms);

  tourist_locations = new ol.layer.Image({
          source: new ol.source.ImageWMS({
            url: 'http://192.168.1.157:8080/geoserver/wms',
            params: {'LAYERS': 'Letchworth_Park:Tourism'},
            zIndex:2,
            ratio: 1,
            serverType: 'geoserver'
          })
        });

  map.addLayer(tourist_locations);



  // campground locations
  // add invisible wms trails for rendering later
  campgroundLocations = new ol.layer.Image({
          source: new ol.source.ImageWMS({
            url: 'http://192.168.1.157:8080/geoserver/wms',
            params: {'LAYERS': 'Letchworth_Park:campground_locations'},
            zIndex:4,
            ratio: 1,
            serverType: 'geoserver'
          })
        });
  map.addLayer(campgroundLocations);

  // add invisible wms trails for rendering later
  trails_highlight_wms = new ol.layer.Image({
          source: new ol.source.ImageWMS({
            url: 'http://192.168.1.157:8080/geoserver/wms',
            params: {'LAYERS': 'Letchworth_Park:trails'},
            zIndex:4,
            ratio: 1,
            serverType: 'geoserver'
          })
        });
  map.addLayer(trails_highlight_wms);

  trailTableJSON = fetchTrailLabels();
  fetchEntranceLabels();
  fetchCampgroundLabels();
  fetchOverlookLabels();
  fetchTourismLabels();

  var collection = new ol.Collection();
  // Modify to select everything with the asset num

    editTrailDataSelection = new ol.interaction.Select({
        layers:[trailLabelFeats],
        condition:ol.events.condition.click,
        features:collection,
        style: function(collector){
          //console.log(collection2);
          return styleHover;

      },
      hitTolerance:30
    });

    editEntranceDataSelection = new ol.interaction.Select({
      layers:[entranceWFS],
      condition:ol.events.condition.click,
      features:collection,
      style: function(collector){
        return styleHover;
      },
      hitTolerance:30
    });

    editCampgroundLocationSelection = new ol.interaction.Select({
      layers:[campgroundWFS],
      condition:ol.events.condition.click,
      features:collection,
      style: function(collector){
        return styleHover;
      },
      hitTolerance:30
    });

    // popup info
    container = document.getElementById('popup');
    content = document.getElementById('popup-content');
    closer = document.getElementById('popup-closer');

    container_campground = document.getElementById('popupCampground');
    content_campground = document.getElementById('popup-contentCampGround');
    closer_campground = document.getElementById('popup-closerCamp');

    campgroundOverlay = new ol.Overlay({
      element:container_campground,
      autoPan:true
    });

    // overlay
    trailOverlay = new ol.Overlay({
      element:container,
      autoPan:true,
    });

    map.addOverlay(trailOverlay);
    map.addOverlay(campgroundOverlay);

    /**
         * Add a click handler to hide the popup.
         * @return {boolean} Don't follow the href.
         */
        closer.onclick = function() {
          trailOverlay.setPosition(undefined);
          closer.blur();
          return false;
        };

        closer_campground.onclick = function() {
          campgroundOverlay.setPosition(undefined);
          closer_campground.blur();
          return false;
        };



}


/*
Return the style when hovering over the hexgrid vector tiles.
*/
function getStyleHover(){
  return new ol.style.Style({
        stroke: new ol.style.Stroke({
          color: '006633',
          width: 5
        }),
      });
}

function getBlankStyle(){
  return new ol.style.Style({
            stroke: new ol.style.Stroke({
            color: 'rgba(255, 255, 255, 0.1)'
          })
        });
}

function getMap(){
  return map;
}



/*
    Used to get the HERE Maps baselayer for the map.
    */
function getHERELayer(){

      var appId = 'szVOwepnSuqM5DKTVGXl';
      var appCode = 'Uch-Z2G-lU0jJXLYCmFO8g';
      var hereLayers =
        {
          base: 'aerial',
          type: 'maptile',
          scheme: 'terrain.day',
          app_id: appId,
          app_code: appCode
        };

      // generate HERE URL for tilemap
      var urlTpl = 'https://1.'+ hereLayers.base + '.maps.cit.api.here.com'+
      '/' + hereLayers.type + '/2.1/maptile/newest/' +
      hereLayers.scheme +'/{z}/{x}/{y}/256/png' +
      '?app_id=' + appId + '&app_code=' + appCode;

      return urlTpl;

}

function fetchTrailLabels(){

  // Get hover property for the trail data
  var vectorSrc = new ol.source.Vector({
    format: new ol.format.GeoJSON(),
    url: function(extent){
      return 'http://192.168.1.157:8080/geoserver/wfs?service=WFS&'
      +'version=1.1.0&request=GetFeature&typename=Letchworth_Park:trails'
      +'&outputFormat=application/json&srsname=EPSG:4326';
    }
  });

  styleHover = getStyleHover();

  // Fetching the geojson of the hex grid for labeling
  trailLabelFeats = new ol.layer.Vector({
    source:vectorSrc,
    style: new ol.style.Style({ // invisible
            stroke: new ol.style.Stroke({
            color: 'rgba(255, 255, 255, 0.1)',
            width:10
          })
        }),
    name: 'letchworth_trail_labels',
    zIndex:20
  });

}

function fetchEntranceLabels(){

  // Get hover property for the trail data
  var vectorSrc = new ol.source.Vector({
    format: new ol.format.GeoJSON(),
    url: function(extent){
      return 'http://192.168.1.157:8080/geoserver/wfs?service=WFS&'
      +'version=1.1.0&request=GetFeature&typename=Letchworth_Park:entrances'
      +'&outputFormat=application/json&srsname=EPSG:4326';
    }
  });


  // Fetching the geojson of the hex grid for labeling
  entranceWFS = new ol.layer.Vector({
    source:vectorSrc,
    style: new ol.style.Style({ // invisible
      image: new ol.style.Circle({
  radius: 7, //7
  fill: new ol.style.Fill({color: 'black'}),
  stroke: new ol.style.Stroke({
    color: [255,0,0], width: 2 //2
      })
    })
        }),
    name: 'letchworth_entrances',
    zIndex:20
  });

  map.addLayer(entranceWFS);

}

function fetchOverlookLabels(){

    // Get hover property for the trail data
    var vectorSrc = new ol.source.Vector({
      format: new ol.format.GeoJSON(),
      url: function(extent){
        return 'http://192.168.1.157:8080/geoserver/wfs?service=WFS&'
        +'version=1.1.0&request=GetFeature&typename=Letchworth_Park:overlooks'
        +'&outputFormat=application/json&srsname=EPSG:4326';
      }
    });

    // Fetching the geojson of the hex grid for labeling
    overlookWFS = new ol.layer.Vector({
      source:vectorSrc,
      style: new ol.style.Style({ // invisible
        image: new ol.style.Circle({
    radius: 7,
    fill: new ol.style.Fill({color: 'rgba(255,0,0,0.1)'}),
    stroke: new ol.style.Stroke({
      color: 'rgba(255,0,0,0.1)', width: 0.1
        })
      })
          }),
      name: 'overlook_locations',
      zIndex:20
    });

    map.addLayer(overlookWFS);


}

function fetchTourismLabels(){

    // Get hover property for the trail data
    var vectorSrc = new ol.source.Vector({
      format: new ol.format.GeoJSON(),
      url: function(extent){
        return 'http://192.168.1.157:8080/geoserver/wfs?service=WFS&'
        +'version=1.1.0&request=GetFeature&typename=Letchworth_Park:Tourism'
        +'&outputFormat=application/json&srsname=EPSG:4326';
      }
    });

    // Fetching the geojson of the hex grid for labeling
    tourist_locations_wfs = new ol.layer.Vector({
      source:vectorSrc,
      style: new ol.style.Style({ // invisible
        image: new ol.style.Circle({
    radius: 7,
    fill: new ol.style.Fill({color: 'rgba(255,0,0,0)'}),
    stroke: new ol.style.Stroke({
      color: 'rgba(255,0,0,0)', width: 0.1
        })
      })
          }),
      name: 'tourist_locations',
      zIndex:20
    });

    map.addLayer(tourist_locations_wfs);


}

function loadLegend(){

  console.log(map.getLayers());

  // doesn't work
  /*
  TODO get each wms for POIS, add as part of layer group,
  THEN implement logic to toggle, include in ssidebar.
  */
  test = new ol.layer.Group({
    title:'test',
    type:'base',
    combine:true,
    visible:false,
    layers:[map.getLayers()]
  });

  console.log(test);
  var sidebar = new ol.control.Sidebar({ element: 'sidebar', position: 'left' });
  var toc = document.getElementById("layers");
  console.log(toc);
  console.log(map);
  ol.control.LayerSwitcher.renderPanel(map, toc);
  map.addControl(sidebar);

}

function fetchCampgroundLabels(){

  // Get hover property for the trail data
  var vectorSrc = new ol.source.Vector({
    format: new ol.format.GeoJSON(),
    url: function(extent){
      return 'http://192.168.1.157:8080/geoserver/wfs?service=WFS&'
      +'version=1.1.0&request=GetFeature&typename=Letchworth_Park:campground_locations'
      +'&outputFormat=application/json&srsname=EPSG:4326';
    }
  });


  // Fetching the geojson of the hex grid for labeling
  campgroundWFS = new ol.layer.Vector({
    source:vectorSrc,
    style: new ol.style.Style({ // invisible
      image: new ol.style.Circle({
  radius: 7,
  fill: new ol.style.Fill({color: 'rgba(255,0,0,0.1)'}),
  stroke: new ol.style.Stroke({
    color: 'rgba(255,0,0,0.1)', width: 0.1
      })
    })
        }),
    name: 'campground_locations',
    zIndex:20
  });

  map.addLayer(campgroundWFS);

}

function clearStyleSelector(){

  // Loop through each trail feature and clear its style
  var trailz = trailLabelFeats.getSource().getFeatures();
  for (i = 0; i < trailz.length; i++){
    var layer = trailz[i];
    layer.setStyle(getBlankStyle);

    }
  }


$(document).ready(createMap);

$(document).ready(function(){

  map.getLayers().forEach(function(layer){
    try {
      console.log(layer.values_.name);

    }
    catch(err){
      console.log(err);
    }
  });

  map.addLayer(trailLabelFeats);

  map.addInteraction(editTrailDataSelection); //here for initial click on layers


      //map.addInteraction(editTrailDataSelection);
      //map.addInteraction(editEntranceDataSelection);
      //map.addInteraction(editCampgroundLocationSelection);

      editTrailDataSelection.on('select',function(e){

        var currFeat = e.selected;
        currentFeatEdit = currFeat; // to hold trail
        //console.log(currFeat[0].values_.name);

        console.log($("#trailNAME"));
        $("#trailNAME")[0].value = currFeat[0].values_.name;
        var currStatus = currFeat[0].values_.status;
        console.log(currStatus);
        $("#comment")[0].value = currFeat[0].values_.notes;

        var trlName = currFeat[0].values_.name;
        var trlLen = currFeat[0].values_.miles;
        var trlSurface = currFeat[0].values_.surface;
        content.innerHTML = '<p>Trail Name: ' + trlName +'</p>' +
        '<p>Length: ' + trlLen + '</p>' + 'Surface: ' + trlSurface + '</p>';
        console.log(currFeat);
        trailOverlay.setPosition(e.mapBrowserEvent.coordinate);


      });

      editEntranceDataSelection.on('select',function(e){
        console.log("Fire");
        var currFeat = e.selected;
        currentFeatEdit = currFeat; // to hold trail
        //console.log(currFeat[0].values_.name);
        console.log(currFeat);

        console.log($("#trailNAME"));
        try{
        $("#trailNAME")[0].value = currFeat[0].values_.asset;
        var currStatus = currFeat[0].values_.status;
        console.log(currStatus);
        $("#comment")[0].value = currFeat[0].values_.notes;
      }catch(err){
        console.log(err);
      }

      });

      editCampgroundLocationSelection.on('select',function(e){
        console.log("Fire for campground");
        console.log(e);
        var currFeat = e.selected;
        currentFeatEdit = currFeat; // to hold trail
        //console.log(currFeat[0].values_.name);
        console.log(currFeat);

        console.log($("#trailNAME"));
        try{
        $("#trailNAME")[0].value = currFeat[0].values_.campground_letter;
        var currStatus = currFeat[0].values_.status;
        console.log(currStatus);
        $("#comment")[0].value = currFeat[0].values_.notes;
      }catch(err){
        console.log(err);
      }
      content_campground.innerHTML = '<p>Letter Name: ' + currFeat[0].values_.campground_letter +'</p>' +
      '<p>Note: ' + currFeat[0].values_.notes + '</p>';
      console.log(currFeat);
      campgroundOverlay.setPosition(e.mapBrowserEvent.coordinate);

      });




  });

  // Listener for finding amenity
  $("#amenityType").click(function(){

    // clear the option parameters
    clearOptions();

    var currButton = $(".btn.btn-primary.focus")[0];
    console.log(currButton);
    console.log(currButton.data);

    // depending on type of feature, populate the dropdown
    if(currButton.id == "trailFeatures"){
      mapVals = enableDropdown(trailLabelFeats,"name");

      // Load up the choppa with trail features
      /*
      var trailOptions = trailLabelFeats.getSource().getFeatures();
      console.log(trailOptions);

      var mapVals = {};

      // for each trail, append name as dropdown option
      for (i = 0; i < trailOptions.length; i++){
        //mapVals.push({key:trailOptions[i].values_.name,
          //value:trailOptions[i].values_.geometry.extent_});
          mapVals[trailOptions[i].values_.name] = trailOptions[i].values_.geometry.extent_;
          var name = trailOptions[i].values_.name
          var geom = trailOptions[i].values_.geometry.extent_
          $("#amenityFinder").append($('<option>', {name,geom})
          .text(name));
      }

      $("#amenityFinder").change(function(){
        // Get from the dictionary
        // Zoom to the feature
        var newValue = $('#amenityFinder').find(":selected");
        console.log(newValue[0].text);
        var extentZoom = mapVals[newValue[0].text];
        map.getView().fit(extentZoom,map.getSize());
      });*/

      //console.log($("#amenityFinder")[0].value);

    }

    else if (currButton.id == "campgroundFeatures"){
      mapVals = enableDropdown(campgroundWFS,"campground_letter");
    }

    else if (currButton.id == "overlookFeatures"){
      mapVals = enableDropdown(overlookWFS, "name");
    }

    else if (currButton.id == "tourismFeatures"){
      mapVals = enableDropdown(tourist_locations_wfs,"asset");
    }

  });

  $("#amenityFinder").change(function(){
    // Get from the dictionary
    // Zoom to the feature
    var newValue = $('#amenityFinder').find(":selected");
    console.log(newValue[0].text);
    console.log(mapVals); // TODO why is this holding onto mapvals from previous check
    var extentZoom = mapVals[newValue[0].text];
    console.log(extentZoom);
    map.getView().fit(extentZoom,{maxZoom:19}); //map.getSize()

  });

  $("#reportType").click(function(){
    console.log("YEEZUS");
    console.log($(".btn.btn-primary.focus"));
    var currButton = $(".btn.btn-primary.focus")[0];
    console.log(currButton);
    console.log(currButton.data);
    if (currButton.id == "trailEdit"){
      // listener for trails
      console.log("Trail Edit");
      map.removeInteraction(editCampgroundLocationSelection);
      map.removeInteraction(editEntranceDataSelection);
      map.addInteraction(editTrailDataSelection);
    }
    else if (currButton.id == "campgroundEdit"){
      // listener for campgrounds
      console.log("Campground Edit");
      map.removeInteraction(editTrailDataSelection);
      map.removeInteraction(editEntranceDataSelection);
      map.addInteraction(editCampgroundLocationSelection);
    }
    else if (currButton.id == "entranceEdit"){
      // listener for entrances
      console.log("Entrance Edit");
      map.removeInteraction(editTrailDataSelection);
      map.removeInteraction(editCampgroundLocationSelection);
      map.addInteraction(editEntranceDataSelection);
    }
  })

  $("#submitReport").click(function(){

    // get value of feature name
    // get operating status entered by user
    // get status
    // send wfs-t request to update operating status and notes to those vals

    // get highlighted value in mapframe
    console.log(currentFeatEdit);
    console.log(currentFeatEdit[0].id_);
    var featID = currentFeatEdit[0].id_;
    var featureStatus = $("#sell")[0].value;
    var commentStatus = $("#comment")[0].value;


    // Get the value from the operating status and comment
    // Pass the currect feature type by splitting the id to get the table name
    var tableName = featID.split(".");
    var featureType = tableName[0];


    updateWFS(featureType, currentFeatEdit, featureStatus, commentStatus);

    // refresh all the wms feeds (why not?)


  })

  $("#campgroundPrimary").click(function(){

    console.log("YEET");


  });


      // listener for slider
      var slider = document.getElementById("myRange");
      console.log(slider);
      var output = document.getElementById("demo")
      output.innerHTML = slider.value;


      slider.oninput = function(){
        console.log(this.value);
        maxTrailDistance = this.value;
        output.innerHTML = slider.value;
        //output.text("Max:" + this.value + " Miles");
      }

    $('#searchTrails').click(function(){
      // Attribute builder
      var testDiff = "difficulty=";
      var finalQuery = "";
      var trailDiff = [];
      var trailActivity = [];
      var activitySelection = "";
      console.log("Searching");
      // Get all the active values for the buttons
      $("#difficulty input[type=checkbox]").each(function(){
        if(($(this).is(':checked'))){

          // The id makes up the query
          difficulty = $(this)[0].name;
          trailDiff.push(testDiff + "'" + difficulty + "' ");


        };
      });

      console.log(trailDiff);

      // For each difficulty, throw an "or" in between
      for (var i = 0; i < trailDiff.length; i++){
        if (i != trailDiff.length - 1){
          // Append or
          finalQuery += trailDiff[i] + "or ";
        }
        else {
          // Append and
          finalQuery += trailDiff[i] + "and ";
        }
      }

      console.log(trailActivity);


      $("#recreation input[type=checkbox]").each(function(){
        if(($(this).is(':checked'))){

          console.log($(this));
          activitySelection = $(this)[0].name;
          trailActivity.push(activitySelection + "= 'Y'");

        }
      });

      console.log(trailActivity.length);

      for (var index = 0; index < trailActivity.length; index++){
        if (index != trailActivity.length - 1){
          // Append or
          finalQuery += trailActivity[index] + "and ";
        }
        else {
          // Append and
          finalQuery += trailActivity[index] + "and ";
        }
      }

      maxTrailDistance = document.getElementById("myRange").value;

      finalQuery += "miles <= " + maxTrailDistance;

      console.log(finalQuery);

      // make the call and return the wms with only those layers
      // highlighted

      refreshWMS(finalQuery);

    });

    $("#clearFilters").click(function(){

      // return the original wms layer
      clearWMSFilter();

      // Select all buttons and clear their active status
      var elems = $(".btn-outline-secondary");

      //document.querySelector("#difficulty > label:nth-child(2)")


      console.log(elems);

      [].forEach.call(elems,function(el){
        console.log(elems);
        console.log(el);
        el.classList.remove("active");
      });

      $(".slider")[0].value = 5.0;

      // listener for slider
      var slider = $('#myRange');
      var output = $('#demo');
      output.text("Max:" + 5.0 + " Miles");

      // set these to unchecked (so they don't recycle old query vals)
      $("#recreation input[type=checkbox]").each(function(){
        if(($(this).is(':checked'))){

          $(this).prop("checked",false);
          console.log($(this).is(':checked'));

        }
      });

      // same deal for difficulty
      $("#difficulty input[type=checkbox]").each(function(){
        if(($(this).is(':checked'))){

          // The id makes up the query
          $(this).prop("checked",false);
          console.log($(this).is(':checked'));

        };
      });




    });

    function refreshWMS(query){

      // Get the wms query from the map

      var newQ = "difficulty='easy'  and bike= 'Y'and xc= 'Y'and horse= 'N'and miles <= 1";

      trails_highlight_wms.getSource().updateParams({'cql_filter': query});
      trails_highlight_wms.getSource().updateParams({'STYLES': 'highlight_trl'});
      trails_highlight_wms.getSource().refresh();
    };

    function clearWMSFilter(){
      console.log("clear");
      trails_highlight_wms.getSource().updateParams({'cql_filter': null});
      trails_highlight_wms.getSource().updateParams({'STYLES': 'Trails_v2'});
      trails_highlight_wms.getSource().refresh();



    };
