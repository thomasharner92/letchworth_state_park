<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd"
  xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

  <NamedLayer>
    <Name>Entrances</Name>
    <UserStyle>
      <Title>status</Title>
      <FeatureTypeStyle>
     <Rule>
       <Name>Open</Name>
       <Title>none</Title>
       <ogc:Filter>
         <ogc:PropertyIsEqualTo>
           <ogc:PropertyName>status_2</ogc:PropertyName>
           <ogc:Literal>open</ogc:Literal>
         </ogc:PropertyIsEqualTo>
       </ogc:Filter>
       <PointSymbolizer>
         <Graphic>
           <Mark>
             <WellKnownName>circle</WellKnownName>
             <Fill>
               <CssParameter name="fill">#008000</CssParameter>
               <CssParameter name="fill-opacity">1</CssParameter>
             </Fill>
             <Stroke>
               <CssParameter name="stroke">#000000</CssParameter>
               <CssParameter name="stroke-width">0</CssParameter>
             </Stroke>
           </Mark>
           <Size>10</Size>
         </Graphic>
       </PointSymbolizer>
       <TextSymbolizer>
         <Label>
            <ogc:PropertyName>asset</ogc:PropertyName>
         </Label>
         <Font>
           <CssParameter name="font-family">Ubuntu</CssParameter>
           <CssParameter name="font-size">10</CssParameter>
           <CssParameter name="font-style">normal</CssParameter>
           <CssParameter name="font-weight">light</CssParameter>
         </Font>
         <LabelPlacement>
           <PointPlacement>
             <AnchorPoint>
               <AnchorPointX>0.5</AnchorPointX>
               <AnchorPointY>0.0</AnchorPointY>
             </AnchorPoint>
             <Displacement>
               <DisplacementX>1</DisplacementX>
               <DisplacementY>1</DisplacementY>
             </Displacement>
           </PointPlacement>
         </LabelPlacement>
       </TextSymbolizer>
     </Rule>
     <Rule>
       <Name>closed</Name>
       <Title>none</Title>
       <ogc:Filter>
           <ogc:PropertyIsEqualTo>
             <ogc:PropertyName>status_2</ogc:PropertyName>
             <ogc:Literal>closed</ogc:Literal>
           </ogc:PropertyIsEqualTo>
       </ogc:Filter>
       <PointSymbolizer>
         <Graphic>
           <Mark>
             <WellKnownName>circle</WellKnownName>
             <Fill>
               <CssParameter name="fill">#FF0000</CssParameter>
               <CssParameter name="fill-opacity">1</CssParameter>
             </Fill>
             <Stroke>
               <CssParameter name="stroke">#000000</CssParameter>
               <CssParameter name="stroke-width">0</CssParameter>
             </Stroke>
           </Mark>
           <Size>10</Size>
         </Graphic>
       </PointSymbolizer>
       <TextSymbolizer>
         <Label>
            <ogc:PropertyName>asset</ogc:PropertyName>
         </Label>
         <Font>
           <CssParameter name="font-family">Ubuntu</CssParameter>
           <CssParameter name="font-size">10</CssParameter>
           <CssParameter name="font-style">normal</CssParameter>
           <CssParameter name="font-weight">light</CssParameter>
         </Font>
         <LabelPlacement>
           <PointPlacement>
             <AnchorPoint>
               <AnchorPointX>0.5</AnchorPointX>
               <AnchorPointY>0.0</AnchorPointY>
             </AnchorPoint>
             <Displacement>
               <DisplacementX>1</DisplacementX>
               <DisplacementY>1</DisplacementY>
             </Displacement>
           </PointPlacement>
         </LabelPlacement>
       </TextSymbolizer>
     </Rule>
   </FeatureTypeStyle>
    </UserStyle>
  </NamedLayer>
</StyledLayerDescriptor>
